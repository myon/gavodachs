"""
Services, cores, and support.

A Service is something that receives a twisted web request (or perhaps
something a bit more digested from formal), processes it into input data using
a ContextGrammar pipes the resulting CoreArgs instance through a core to
receive a data set and optionally tinkers with that data set.

A core receives a CoreArgs thing, processes it, and returns something.
That something typically is a table (an instance of something derived
from rsc.BaseTable), and if it is, then service will adapt it
to its outputTable (if necessary), wrap it into an rsc.Data instance
and return it.

Other core results will be handed through as-is, ignoring the service's 
outputTable.  There are two things many renderers understand: For one,
complete rsc.Data instances, which you'll want if your core produces
multi-table results.  And secondly, a pair of (media type, bytes), which
instructs several renderers to write bytes to the client with with 
content-type set to media type.


Support code is in common.  Most importantly, this is QueryMeta, a data
structure carrying lots of information on the query being processed.
"""

#c Copyright 2008-2020, the GAVO project
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


# Not checked by pyflakes: API file with gratuitous imports

from gavo.svcs.common import (Error, UnknownURI, ForbiddenURI, WebRedirect,
	RedirectBase, SeeOther, Authenticate, BadMethod, Found,
	QueryMeta, emptyQueryMeta, getTemplatePath, loadSystemTemplate)

from gavo.svcs.core import getCore, Core, CORE_REGISTRY

from gavo.svcs.customcore import CustomCore

from gavo.svcs.customwidgets import (DBOptions, FormalDict, 
	SimpleSelectChoice, 
	NumericExpressionField, DateExpressionField, StringExpressionField, 
	ScalingTextArea)

from gavo.svcs.inputdef import (
	CoreArgs, InputTD, InputKey, ContextGrammar)

from gavo.svcs.outputdef import OutputField, OutputTableDef

from gavo.svcs.renderers import RENDERER_REGISTRY, getRenderer

from gavo.svcs.service import (Service, Publication, PreparsedInput)

from gavo.svcs.standardcores import (DBCore, CondDesc,
	mapDBErrors)

from gavo.svcs.uploadcores import UploadCore

from gavo.svcs.vanity import getVanityMap
