"""
This should evolve into a useful API to GAVO code for more-or-less external
clients.
"""

#c Copyright 2008-2020, the GAVO project
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


# Not checked by pyflakes: API file with gratuitous imports

# this (and its companion in user/cli.py) works around a race condition in
# Debian stretch's python-cryptography module.  Remove about 2021.
from cryptography.hazmat.bindings.openssl.binding import Binding 

from gavo import base
from gavo import rscdesc
from gavo import votable
from gavo import web

getRD = base.caches.getRD
RD = rscdesc.RD

from gavo.base import (getConfig, setConfig, getBinaryName,
	getDBConnection, DBError,	IntegrityError,
	UnmanagedQuerier, AdhocQuerier, 
	getTableConn, getAdminConn, getUntrustedConn,
	getWritableTableConn, getWritableAdminConn,
	NoMetaKey, Error, StructureError, ValidationError, LiteralParseError, 
	ReportableError, NotFoundError, RDNotFound, SourceParseError, DataError,
	MetaValidationError, BadUnit, BadCode,
	parseFromString,
	makeStruct,
	parseUnit,
	getMetaText,
	ui,
	resolveCrossId, getTableDefForTable)

from gavo.formats import formatData, getFormatted
from gavo.formats.votablewrite import (writeAsVOTable, getAsVOTable,
	VOTableContext)

from gavo.helpers.processing import (CannotComputeHeader,
	FileProcessor, ImmediateHeaderProcessor, HeaderProcessor,
	AnetHeaderProcessor, PreviewMaker, SpectralPreviewMaker,
	procmain)

from gavo.helpers.fitstricks import (addHistoryCard,
	updateTemplatedHeader, makeHeaderFromTemplate, getTemplateForName)

from gavo.rsc import (TableForDef, DBTable, makeData, parseValidating,
	parseNonValidating, getParseOptions, Data, makeDependentsFor,
	createDump, restoreDump)

from gavo.rscdef import TableDef, getFlatName, getReferencedElement

from gavo.stc import (dateTimeToJYear, dateTimeToJdn, dateTimeToMJD,
	jYearToDateTime, jdnToDateTime, mjdToDateTime, parseISODT)

from gavo.svcs import (UnknownURI, ForbiddenURI, Authenticate, 
	WebRedirect, SeeOther, Core, OutputTableDef, QueryMeta)

from gavo.user.logui import LoggingUI
from gavo.user.plainui import StingyPlainUI, PlainUI

from gavo.utils import (
	loadPythonModule, formatISODT, bytify, EqualingRE,
	pyfits, cutoutFITS,
	safeReplaced)

from gavo.votable import VOTableError, ADQLTAPJob

from gavo.web import ServiceBasedPage, renderDCErrorPage
from gavo.helpers.testtricks import getXMLTree

from gavo.rscdef.rmkfuncs import *

__version__ = base.getVersion()
