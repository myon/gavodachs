"""A package (for Nevow) for defining the schema, validation and rendering of
HTML forms.
"""

# Not checked by pyflakes: API file with gratuitous imports

version_info = (1, 0, 0)
version = '.'.join([str(i) for i in version_info])

from twisted.web import iweb
from twisted.web import static
from . import twistedpatch

from .nevowc import (XMLFile, XMLString, 
    addNevowAttributes,
    TemplatedPage, NevowcElement, 
    flattenSync)
from .types import *
from .validation import *
from .widget import *
from .widgets.multiselect import *
from .form import Form, Field, Group, ResourceWithForm, FORMS_KEY
from . import iformal

def widgetFactory(widgetClass, *a, **k):
    def _(original):
        return widgetClass(original, *a, **k)
    return _

# Register standard adapters
from twisted.python.components import registerAdapter
from . import converters
from .util import SequenceKeyLabelAdapter, StringableLabelAdapter
registerAdapter(TextInput, String, iformal.IWidget)
registerAdapter(TextInput, Integer, iformal.IWidget)
registerAdapter(TextInput, Float, iformal.IWidget)
registerAdapter(Checkbox, Boolean, iformal.IWidget)
registerAdapter(DatePartsInput, Date, iformal.IWidget)
registerAdapter(TextInput, Time, iformal.IWidget)
registerAdapter(FileUpload, File, iformal.IWidget)
registerAdapter(TextAreaList, Sequence, iformal.IWidget)
registerAdapter(SequenceKeyLabelAdapter, tuple, iformal.IKey)
registerAdapter(SequenceKeyLabelAdapter, tuple, iformal.ILabel)
registerAdapter(StringableLabelAdapter, bytes, iformal.ILabel)
registerAdapter(StringableLabelAdapter, str, iformal.ILabel)
registerAdapter(StringableLabelAdapter, int, iformal.ILabel)
registerAdapter(converters.ForceStringConverter, String, iformal.IStringConvertible)
registerAdapter(converters.DateToDateTupleConverter, Date, iformal.IDateTupleConvertible)
registerAdapter(converters.BooleanToStringConverter, Boolean, iformal.IBooleanConvertible)
registerAdapter(converters.BooleanToStringConverter, Boolean, iformal.IStringConvertible)
registerAdapter(converters.IntegerToStringConverter, Integer, iformal.IStringConvertible)
registerAdapter(converters.FloatToStringConverter, Float, iformal.IStringConvertible)
registerAdapter(converters.DateToStringConverter, Date, iformal.IStringConvertible)
registerAdapter(converters.TimeToStringConverter, Time, iformal.IStringConvertible)
registerAdapter(converters.NullConverter, File, iformal.IFileConvertible)
registerAdapter(converters.NullConverter, Sequence, iformal.ISequenceConvertible)
registerAdapter(converters.SequenceToStringConverter, Sequence, iformal.IStringConvertible)

try:
    Decimal
except NameError:
    pass
else:
    registerAdapter(TextInput, Decimal, iformal.IWidget)
    registerAdapter(converters.DecimalToStringConverter, Decimal, iformal.IStringConvertible)
del SequenceKeyLabelAdapter
del registerAdapter
