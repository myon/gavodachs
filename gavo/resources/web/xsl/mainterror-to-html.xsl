<?xml version="1.0" encoding="UTF-8"?>

<!-- A stylesheet formatting maintenance message (which are given in
DALI format so things work out nicely in non-browser clients) 

This stylesheet is made available under CC-0 by the GAVO project,
http://www.g-vo.org.  
See http://creativecommons.org/publicdomain/zero/1.0/ for details.
-->

<xsl:stylesheet
    xmlns:v="http://www.ivoa.net/xml/VOTable/v1.3"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
    version="1.0">
   
   	<xsl:include href="dachs-xsl-config.xsl"/>

		<xsl:output method="xml" 
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>


		<xsl:template match="text()"/>

		<xsl:template match="v:INFO[@name='QUERY_STATUS']">
			<pre class="errmsg">
				<xsl:value-of select="."/>
			</pre>
		</xsl:template>

		<xsl:template match="/">
			<html>
			<head>
				<title>GAVO DC: Down for Maintenance</title>
				<style type="text/css">
					pre.errmsg {
						margin-top: 1ex;
						border-top: 2pt solid red;
						padding-top: 0.5ex;
						padding-bottom: 0.5ex;
						border-bottom: 2pt solid red;
						margin-bottom: 1ex
					}
				</style>
			</head>
			<body>
				<h1>Maintenance</h1>

				<p>We are terribly sorry, but the archive serivce is currently down
				for maintenance or because of severe technical trouble.</p>

				<p>The reason given by the operators is:</p>

				<xsl:apply-templates/>

				<p>We generally have something on
				<a href="https://blog.g-vo.org">our blog</a> if trouble
				lasts longer than an hour or so.</p>
				</body>
			</html>
		</xsl:template>
</xsl:stylesheet>


