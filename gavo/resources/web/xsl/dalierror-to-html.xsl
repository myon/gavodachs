<?xml version="1.0" encoding="UTF-8"?>

<!-- A stylesheet formatting DALI-style error messages in INFO elements
to look halfway reasonable in a browser window.

This stylesheet is made available under CC-0 by the GAVO project,
http://www.g-vo.org.  
See http://creativecommons.org/publicdomain/zero/1.0/ for details.
-->

<xsl:stylesheet
    xmlns:v="http://www.ivoa.net/xml/VOTable/v1.3"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
    version="1.0">
   
   	<xsl:include href="dachs-xsl-config.xsl"/>

		<xsl:output method="xml" 
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>


		<xsl:template match="text()"/>

		<xsl:template match="v:INFO[@name='QUERY_STATUS']">
			<pre class="errmsg">
				<xsl:value-of select="."/>
			</pre>
		</xsl:template>

		<xsl:template match="v:INFO[@name='HINT']">
			<p>The error came with a hint that may be helpful when fixing
			things:</p><p><em><xsl:value-of select="."/></em></p>
		</xsl:template>

		<xsl:template match="/">
			<html>
			<head>
				<title>DaCHS DALI Error</title>
				<xsl:call-template name="localCompleteHead"/>
				<style type="text/css">
					pre.errmsg {
						margin-top: 1ex;
						border-top: 2pt solid red;
						padding-top: 0.5ex;
						padding-bottom: 0.5ex;
						border-bottom: 2pt solid red;
						margin-bottom: 1ex
					}
				</style>
			</head>
			<body>
				<h1>DaCHS DALI Error</h1>

				<p>Your request caused an error.  This could be because you
				put in bad parameters or because something is broken at our end.
				Here is the reason given for the failure:</p>

				<xsl:apply-templates/>

				<xsl:call-template name="localMakeFoot"/>
				</body>
			</html>
		</xsl:template>
</xsl:stylesheet>


