<resource resdir="__system" schema="dc">
	<meta name="creationDeate">2020-07-23T14:00:00</meta>
	<meta name="description">Table structures and other RD material
		for serialising time series.</meta>
	
	<mixinDef id="phot-0">
		<doc>
			*Warning:* The specs here are strongly in flux.  The interface here
			is quite likely to change.  If you use this mixin, please tell
			gavo@ari.uni-heidelberg.de so we can inform you of incompatible
			changes, and make sure you have robust regression tests in place.

			A mixin for a simple photometric time series.  Mix this in to
			get columns obs_time and phot_val, properly declared for
			the `2020 time series note`_.

			Parameters marked with “SIL literal” can take a C-style token, a
			quoted string, or a reference to a column or param, where the name is
			prefixed by an @.

			.. _2020 time series note: http://ivoa.net/documents/Notes/LightCurveTimeSeries/
		</doc>

		<mixinPar key="timescale" description="Time scale of the time coordinate.
			Choose a value from http://www.ivoa.net/rdf/timescale (SIL literal)"/>
		<mixinPar key="refposition" description="Reference position for the time
			coordinate.  Choose a value from http://www.ivoa.net/rdf/refposition
			(SIL literal)."/>
		<mixinPar key="time0" description="JD of the epoch (“day 0”) of the time 
			coordinate; this is 0 for JD itself, 2400000.5 for MJD (SIL literal)."/>

		<mixinPar key="refframe" description="Frame of coordinates you may be
			giving (SIL literal).">__NULL__</mixinPar>
		<mixinPar key="pos_epoch" description="Epoch you give the position for
			(SIL literal).">__NULL__</mixinPar>
		<mixinPar key="longitude" description="Longitude/RA of the position
			(SIL literal; you have to define params/columns yourself if you
			want to reference something here).">__NULL__</mixinPar>
		<mixinPar key="latitude" description="Latitude/Dec of the position
			(SIL literal; you have to define params/columns yourself if you want
			to reference something here).">__NULL__</mixinPar>

		<mixinPar key="independent_axes" description="Axes that describe the
			 time (only change if you define ones in addition to the obs_time
			 provided by the mixin; SIL literal)">[@obs_time]</mixinPar>
		<mixinPar key="dependent_axes" description="Axes with values varying
			with time (only change if you define ones in addition to phot provided
			by the mixin; SIL literal)">[@phot]</mixinPar>

		<mixinPar key="filterIdentifier" description="Identifier for the
			passband the photometry is for (as defined by the Note).  This is a
			SIL literal, and since it will probably contain characters like '/',
			will likely have to quote it as in 
			``filterIdentifier='&#x22;Gaia/G&#x22;'``."/>
		<mixinPar key="zeroPointFlux" description="Flux at the given zero point,
			in Jy; SIL literal).">__NULL__</mixinPar>
		<mixinPar key="magnitudeSystem" description="Magnitude system used: Vega,
			AB,... (SIL literal)">__NULL__</mixinPar>
		<mixinPar key="effectiveWavelength" description="Central wavelength
			(or similar measure) for the passband used for the photometry, in
			meters (SIL literal)"/>
		
		<mixinPar key="time_description" description="Description 
			of the time coordinate.  We assume it's in days; if you have something
			else, override the obs_time coordinate as described in `mixins`_."
			>Time</mixinPar>
		<mixinPar key="phot_description" description="Description 
			of the photometric coordinate."
			>Flux</mixinPar>
		<mixinPar key="phot_unit" description="Unit of the photometric
			coordinate; something like mag, Jy, 1e-20 W/(Hz.m**2)."/>
		<mixinPar key="phot_ucd" description="UCD of the photometric
			coordinate; something like phot.mag;em.opt.V or 
			phot.flux;em.x-ray.hard"/>

		<events>
			<dm>
				(stc2:Coords) {
					time: (stc2:TimeCoordinate) {
						frame:
							(stc2:TimeFrame) {
								timescale: \timescale
								refPosition: \refposition 
								time0: \time0 }
						location: @obs_time
					}
					space: 
						(stc2:SphericalCoordinate) {
							frame: (stc2:SpaceFrame) {
								orientation: \refframe
								epoch: \pos_epoch }
							longitude: \longitude
							latitude: \latitude
						}
				}
			</dm>

			<dm>
				(ndcube:Cube) {
					independent_axes: \independent_axes
					dependent_axes: \dependent_axes
				}
			</dm>

			<dm>
				(phot:PhotCal) {
					filterIdentifier: \filterIdentifier
					zeroPointFlux: \zeroPointFlux
					magnitudeSystem: \magnitudeSystem
					effectiveWavelength: \effectiveWavelength
					value: @phot
				}
			</dm>

			<param name="dataproduct_type" type="text"
				ucd="meta.code.class"
				utype="obscore:ObsDataset.dataProductType">timeseries</param>
			<param name="dataproduct_subtype" type="text"
				ucd="meta.code.class"
				utype="obscore:ObsDataset.dataProductSubtype">lightcurve</param>

			<column name="obs_time" type="double precision"
				unit="d" ucd="time.epoch"
				tablehead="Time"
				description="\time_description"/>
			<column name="phot" type="double precision"
				unit="\phot_unit" ucd="\phot_ucd"
				tablehead="Phot"
				description="\phot_description"/>
		</events>
		
	</mixinDef>
</resource>
