<resource schema="dc" resdir="__system">
	<meta name="creationDate">2020-02-21</meta>
	<meta name="description">Management of RDs on the system.</meta>
	<meta name="subject">astronomical-software</meta>

	<table id="rdmeta" onDisk="True" system="True"
			dupePolicy="overwrite" primary="sourceRD">
		<meta name="description">This table lists the RDs DaCHS has imported
			or otherwise manipulated.

			Additionally, on import, we add the schema and whether there's
			ADQL tables in the RD; this helps when several RDs share a single
			schema.
		</meta>

		<column name="sourceRD" type="text"
			tablehead="RD id"
			description="Identifier (i.e., inputs-relative path, without .rd)
				of the RD"
			verbLevel="1"/>
		<column name="data_updated" type="timestamp"
			tablehead="Data Upd."
			description="UTC of last execution of dachs imp on this RD.  This
				is NULL for RDs without data or when they have never been imported.
				This is *not* reset on drop."
			verbLevel="1"/>
		<column name="schema_name" type="text"
			tablehead="Schema"
			description="Name of the schema managed by this RD when it was
				 last touched."
			verbLevel="1"/>
		<column name="adql" type="boolean" required="True"
			tablehead="TAP?"
			description="True if the RD had TAP-published tables when it was
				 last touched."
			verbLevel="1"/>
	</table>

	<data id="update-for-rd" updating="True">
		<!-- this data element takes a pair of (reason, RD) as input to its
			embedded grammar and creates an new record for rdmeta.  It is
			made manually by dachs imp and dachs pub.  -->
		<sources/>
		<embeddedGrammar>
			<iterator>
				<code>
					reason, rd = self.sourceToken
					row = {
						"sourceRD": rd.sourceId,
						"schema_name": rd.schema,
						"data_updated": rd.getMeta("dataUpdated"),
					}

					for td in rd.tables:
						if td.adql:
							row["adql"] = True
							break
					else:
						row["adql"] = False
					
					if reason=="import":
						row["data_updated"] = datetime.datetime.utcnow()

					yield row
				</code>
			</iterator>
		</embeddedGrammar>
		<make table="rdmeta"/>
	</data>
</resource>
