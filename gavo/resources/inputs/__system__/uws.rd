<resource schema="uws" resdir="__system">
	<meta name="creationDate">2009-12-04T09:00:00</meta>
	<meta name="subject">virtual-observatories</meta>
	<meta name="description">Helpers for the support of 
	universial worker services.</meta>
	<!-- the uwsfields STREAM contains what's necessary for
	protocols.uws.BaseUWSJob; in particular, don't mess with the table
	attributes given at the top. -->

	<STREAM id="uwsfields">
		<onDisk>True</onDisk>
		<primary>jobId</primary>
		<allProfiles>feed,trustedquery</allProfiles>
		<dupePolicy>overwrite</dupePolicy>
		<!-- the entire UWS shares a single directory for the job directories. -->
		<column name="jobId" type="text" 
			description="Internal id of the job.  At the same time, 
				uwsDir-relative name of the job directory."/>
		<column name="phase" type="text" 
			description="The state of the job.">
			<values>
				<option>PENDING</option>
				<option>QUEUED</option>
				<option>EXECUTING</option>
				<option>COMPLETED</option>
				<option>ERROR</option>
				<option>ABORTED</option>
				<option>UNKNOWN</option>
			</values>
		</column>
		<column name="executionDuration" type="integer"
			unit="s"
			description="Job time limit">
			<values nullLiteral="-1"/>
		</column>
		<column name="destructionTime" type="timestamp"
			description="Time at which the job, including ancillary 
			data, will be deleted"/>
		<column name="owner" type="text" 
			description="Submitter of the job, if verified"/>
		<column name="parameters" type="text" 
			description="Pickled representation of the parameters (except uploads)"/>
		<column name="runId" type="text" 
			description="User-chosen run Id"/>
		<column name="startTime" type="timestamp" 
			description="UTC job execution started"/>
		<column name="endTime" type="timestamp" 
			description="UTC job execution finished"/>
		<column name="error" type="text"
			description="some suitable representation an error that has
			occurred while executing the job (null means no error information
			has been logged)"/>
		<column name="creationTime" type="timestamp"
			description="UTC job was created"/>
	</STREAM>

	<STREAM id="uwsargs">
		<doc>Arguments for UWS requests.  This needs to be fed into the
		parameter definition of every UWS service.</doc>
		<!-- this is currently parsed during method dispatch (getChild)
		and thus should not raise ValidationErrors (which will become 500s
		rather than 400s).  Hence, don't use DaCHS' validation features. -->

		<inputKey name="upload" type="file"
			multiplicity="multiple"
			description="A DALI-type upload string"/>
		<inputKey name="runid" type="text"
			description="Ignored by this service."/>
		<inputKey name="action" type="text"
			multiplicity="single"
			description="On posts to the job URI, you can pass in DELETE here
				instead of using the standards-compliant DELETE method.
				Ignored otherwise."/>
		<inputKey name="wait" type="text"
			unit="s"
			multiplicity="single"
			description="On certain operations, pass a timeout here to
				have the server delay its response until there is a change
				to report."/>
		<inputKey name="phase" type="text"
			multiplicity="single"
			description="A UWS PHASE; the interpretation is different across
				the UWS operations."/>
		<inputKey name="executionduration" type="text"
			unit="s"
			multiplicity="single"
			description="When posting to the job resource, set the maximum
				run time of the query to this."/>
		<inputKey name="destruction" type="text"
			multiplicity="single"
			description="When posting to the job resource, adjust the
				time at which the job will be thrown away to this."/>

	</STREAM>


	<!-- have an empty data so gavo imp does not complain -->
	<data id="empty"/>

	<data id="upgrade_0.6.3_0.7" auto="false">
		<!-- remove old uws tables; this needs a special script because
		the drop functionality was broken for system tables before 0.7 -->
		<sources items="0"/>
		<nullGrammar/>
		<make>
			<table onDisk="True" temporary="True" id="tmp"/>
			<script lang="AC_SQL" name="drop old UWS tables" type="newSource">
				drop table uws.jobs;
				drop table uws.uwsresults;
				delete from dc.tablemeta where tablename='uws.jobs';
				delete from dc.tablemeta where tablename='uws.uwsresults';
				delete from dc.columnmeta where tablename='uws.jobs';
				delete from dc.columnmeta where tablename='uws.uwsresults';
			</script>
		</make>
	</data>


	<table id="userjobs" onDisk="True" system="True">
		<meta name="description">The jobs table for user-defined UWS
		jobs. As the jobs can come from all kinds of services, this
		must encode the jobClass (as the id of the originating service).
		</meta>

		<FEED source="uwsfields"/>
		<column name="pid" type="integer" 
				description="A unix pid to kill to make the job stop">
			<values nullLiteral="-1"/>
		</column>
		<column name="jobClass" type="text"
			description="Key for the job class to use here.  This is,
				as an implementation detail, simply the cross-id of the service
				processing this."/>
	</table>

	<data id="enable_useruws" auto="False">
		<make table="userjobs"/>
	</data>

</resource>
