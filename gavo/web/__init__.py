"""
Code for talking to http clients.
"""

#c Copyright 2008-2020, the GAVO project
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


# Not checked by pyflakes: API file with gratuitous imports

from gavo.web.common import Request, compwrap, BinaryItem

from gavo.web.grend import (GavoRenderMixin, ServiceBasedPage, 
	CustomTemplateMixin)

from gavo.web.weberrors import renderDCErrorPage
