#!/usr/bin/python3

#c Copyright 2008-2020, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.

import os
import sys

from setuptools import setup, find_packages, Extension

VERSION = "2.3"


install_requires = [
# the dependencies are commented out because I think it's an abomination
# that an installer script pulls and installs packages from half of the 
# internet.  But if you're not on Debian and don't mind pip madness, 
# uncomment the following and you should get a working DaCHS.
# (this isn't tested regularly, so I depend on your bug reports if 
# something's missing)
#	"astropy", "cryptography", "docutils", "lxml", "matplotlib", "numpy", "Pillow", "pkg-resources", "psycopg2", "pymoc", "pyparsing", "rjsmin", "testresources", "twisted",
]


SETUP_ARGS = {
	"name": "gavodachs",
	"description": "ZAH GAVO data center complete package",
	"url": "http://vo.ari.uni-heidelberg.de/soft",
	"license": "GPL",
	"author": "Markus Demleitner",
	"author_email": "gavo@ari.uni-heidelberg.de",
	"packages": find_packages(),
	# We're not zip-safe because the XSD validator accesses dist files by path
	"zip_safe": False,
	"include_package_data":  True,
	"install_requires": install_requires,
	"entry_points": {
		'console_scripts': [
			'gavo = gavo.user.cli:main',
			'dachs = gavo.user.cli:main',
		]
	},
	"version": VERSION,
}

if __name__=="__main__":
	setup(**SETUP_ARGS)
